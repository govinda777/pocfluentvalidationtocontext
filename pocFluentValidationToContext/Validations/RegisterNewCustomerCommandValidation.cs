using pocFluentValidationToContext.Models;

namespace pocFluentValidationToContext.Validations
{
    public class RegisterNewCustomerCommandValidation: CustomerValidation<CustomerModel>
    {
        public RegisterNewCustomerCommandValidation()
        {
            ValidateName();
            ValidateBirthDate();
            ValidateEmail();
        }
    }
}