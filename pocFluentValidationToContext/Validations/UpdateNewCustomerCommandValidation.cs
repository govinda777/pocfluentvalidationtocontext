using pocFluentValidationToContext.Models;

namespace pocFluentValidationToContext.Validations
{
    public class UpdateNewCustomerCommandValidation : CustomerValidation<CustomerModel>
    {
        public UpdateNewCustomerCommandValidation()
        {
            ValidateName();
        }
    }
}