using System;

namespace pocFluentValidationToContext.Models
{
    public class CustomerModel
    {
        public Guid Id { get; protected set; }

        public string Name { get; protected set; }

        public string Email { get; protected set; }

        public DateTime BirthDate { get; protected set; }
    }
}