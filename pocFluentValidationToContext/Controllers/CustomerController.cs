﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using pocFluentValidationToContext.Models;
using pocFluentValidationToContext.Validations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace pocFluentValidationToContext.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        // GET: api/values
        [HttpGet]
        public ActionResult Get()
        {
            var model = new CustomerModel();

            var validationResult = new RegisterNewCustomerCommandValidation().Validate(model);

            return Ok(validationResult);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var model = new CustomerModel();

            var validationResult = new UpdateNewCustomerCommandValidation().Validate(model);

            return Ok(validationResult);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
